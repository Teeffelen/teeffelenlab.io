---
layout: default
title: "About"
description: "Lex van Teeffelen"
sitemap: true
---

Hey 👋

I'm Lex and I'm a curious and tech-savvy Software Engineer from The Netherlands.

I usually go by the username **@teeffelen** online, you can find me on:

- [GitLab] -- My current git platform, mostly private repo's
- [GitHub] -- Contains older repositories, not really active
- [Printables] -- Things I make and design for 3D printing
- [LinkedIn] -- I may or may not reply here

![]({% link /assets/img/me.jpg %}){:height="863px" width="1080px"}
_Enjoying a glass of Sangria (September 2022)_

[GitHub]: https://github.com/teeffelen
[GitLab]: https://gitlab.com/teeffelen
[LinkedIn]: https://nl.linkedin.com/in/lex-van-teeffelen
[Printables]: https://www.printables.com/social/90980-lex/about
