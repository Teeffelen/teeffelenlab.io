#!/bin/bash
#
# Opens a ruby container that builds and serves your Jekyll site at:
#   http://localhost:4000/
#
# Usage:
#   ./host.sh [Jekyll serve options]
#

set -euo pipefail

readonly IMAGE="docker.io/library/ruby:3"
readonly WORKDIR="/srv/jekyll"
readonly BUNDLE_PATH="_cache"
readonly DESTINATION="public"

# Runs a command in the ruby container
#   $1 - the command to run
podman_run() {
    # shellcheck disable=2086
    podman run \
        --rm \
        --interactive \
        --tty \
        --volume "${PWD}:${WORKDIR}:z" \
        --workdir "${WORKDIR}" \
        --publish "4000:4000" \
        --publish "35729:35729" \
        --env BUNDLE_PATH="${BUNDLE_PATH}" \
        "${IMAGE}" \
        $1
}

# Check if dependencies are cached, if not, install them
if [[ ! -d "${BUNDLE_PATH}" ]]; then
    podman_run "bundle install"
fi

# Build and serve the jekyll page
podman_run "
    bundle exec jekyll serve
        --destination ${DESTINATION}
        --livereload
        --host 0.0.0.0
        --watch ${1:-}"
