# Provides `next_in_category` and `previous_in_category` for each post in a
# given category. Used to add the `next` and `previous` buttons at the bottom
# of posts.
#
module Jekyll
    class WithinCategoryPostNavigation < Generator
        def generate(site)
        site.categories.each_pair do |category, posts|
            posts.sort! { |a,b| b <=> a}
            posts.each do |post|
            index = posts.index post
            next_in_category = nil
            previous_in_category = nil
            if index
                if index < posts.length - 1
                    next_in_category = posts[index + 1]
                end
                if index > 0
                    previous_in_category = posts[index - 1]
                end
            end
            post.data["next_in_category"] = next_in_category unless next_in_category.nil?
            post.data["previous_in_category"] = previous_in_category unless previous_in_category.nil?
            end
        end
        end
    end
end
