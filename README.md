# teeffelen.gitlab.io

Personal website using GitLab Pages and Jekyll.
View it live at: https://teeffelen.gitlab.io/

## Building locally

```bash
# build and run the site locally (tracks changes with --watch)
./host.sh

# (optionally) include drafts from the `_drafts` directory
./host.sh --drafts
```

## Licensing

- [Jekyll](https://github.com/jekyll/jekyll/blob/master/LICENSE) (MIT 2008)
- [New.css](https://github.com/xz/new.css/blob/master/LICENSE) (MIT 2020)
- [Unsplash](https://unsplash.com/license)
