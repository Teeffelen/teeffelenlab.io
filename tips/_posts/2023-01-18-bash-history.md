---
layout: post
title: "Bash History"
description: "How to go incognito 🕵️"
tags: [linux, tips]
---

You can hide a command from history by prepending it with a space:

```bash
echo "I will end up in history :("
 echo "I will be hidden! :)"
```

This is useful when you want to quickly try a command, without it filling up
Bash history.
It can also be useful to quickly view a help or man page, as follows:

```bash
  git help
  man bash | less +'/HISTCONTROL'
```

Make sure you have `HISTCONTROL` set to `ignorespace` or `ignoreboth`
in your `.bashrc` for this to work:

```bash
# ~/.bashrc

HISTCONTROL=ignoreboth
```

Or if you're using ZSH, add the following to your `.zshrc` file:

```zsh
# ~/.zshrc

setopt HIST_IGNORE_SPACE
```

> **Tip:** This also works for most other shells, check the documentation on
> how to set it up in your preferred shell!
