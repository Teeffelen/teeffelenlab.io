---
layout: post
title: "Style Guides"
description: "Write code in a consistent style"
tags: [linux, tips, guide, programming]
---

Ever struggled with formatting a piece of code?
This might be your solution!

Google created a list of [style guides for programming languages].

The list covers most common programming/scripting languages, such as:

- [Shell/bash scripts]
- [Python]
- [C++]
- [HTML/CSS]

Browse through them before writing your next piece of code!

Keep in mind that certain projects may have their own style guides in place.
It's best to stay consistent with the existing code style!

I also recommend watching [Seven Ineffective Coding Habits] by Kevlin Henney.

[style guides for programming languages]: https://google.github.io/styleguide
[Shell/bash scripts]: https://google.github.io/styleguide/shellguide.html
[Python]: https://google.github.io/styleguide/pyguide.html
[C++]: https://google.github.io/styleguide/cppguide.html
[HTML/CSS]: https://google.github.io/styleguide/htmlcssguide.html
[Seven Ineffective Coding Habits]: {% link blog/_posts/2023-01-13-ineffective-coding-habits.md %}
