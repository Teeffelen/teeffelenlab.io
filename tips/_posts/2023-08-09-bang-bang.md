---
layout: post
title: "Bang bang commands"
description: "A must-know trick for Linux 🔫"
tags: [linux, tips]
---

Ever forget to issue `sudo` before a command? Then you might want to
learn about bang (`!`) commands on Linux.

Here are three common examples of what you can do with bash bang.

## Repeat last command

Repeat the last command in the shell's history by using a double bang (`!!`).
Useful if you forgot to issue `sudo` before a command.

```bash
$ dnf upgrade
Error: This command has to be run with superuser privileges.

$ sudo !!
 sudo dnf upgrade
...
```

## Repeat by string’s beginning

A bang (`!`) followed by the first character (or string) matching the
command you want to run will repeat that command.

```bash
$ ls -A
dir  dir1  file  file1  .hello.txt

$ echo "another command"
another command

$ !ls
 ls -A
dir  dir1  file  file1  .hello.txt
```

## Repeat by matching a string

The `!?<string>` bang format does the same as the above, but `<string>`
doesn’t have to be at the beginning of the command.

```bash
$ cat hello.txt
Hello world!

$ !?hello
 cat hello.txt
Hello world!
```

## And more

There are actually way more useful bang commands (`!`) you can use on Linux.

I recommend reading the [Red Hat article about "bang bang commands"][article]
for a more detailed explanation.

[article]: https://www.redhat.com/sysadmin/bash-bang-commands
