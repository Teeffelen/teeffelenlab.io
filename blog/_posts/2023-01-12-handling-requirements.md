---
layout: post
title: "Handling Requirements"
description: "Elon Musk's five step process"
tags: []
---

1. Make your requirements less dumb
2. Delete the part or process
3. Simplify or optimize
4. Accelerate cycle times
5. Automate

{% include youtube.html id="hhuaVsOAMFc" %}
