---
layout: post
title: "Autonomous Car Racing"
description: "Exploring neural networks with Java"
tags: [ai, java, neural network]
---

I programmed a self-driving car using a neural network in Java for a school
project. We used [The Open Racing Car Simulator][TORCS] (TORCS), which is a
racing game that is often used as a platform to create self-driving cars.
The TORCS source code is open source, which gives developers the ability to
create their own cars and drivers. The self-created drivers can compete in
races against TORCS-made drivers, or even other clients. A server is required
in order for a machine to connect with the game.

[TORCS]: https://en.wikipedia.org/wiki/TORCS

## Neural Network

The artificial neural network used to control the car is a
[feedforward neural network][FNN] that uses [resilient backpropagation][Rprop]
as an optimization function. The realized program uses the
[Java Encog library][encog] by Jeff Heaton to create and manage the neural
network. The Encog library implements different types of neural networks,
[activation functions][AF], and training techniques that are required in this
project.

The network consists of twenty-two input neurons, these take floating point
input values ranging from 0.0 to 1.0. These input neurons are connected to a
hidden layer with certain weights, this hidden layer consists of nineteen
neurons that will 'fire' a value ranging from -1.0 to 1.0 to the three
output neurons.

The last three neurons decide and control the car's actions;

- Steering
- Accelerating
- Braking

![Neural network](/assets/img/neural-network.png){:width="1204px" height="924px"}

[FNN]: https://en.wikipedia.org/wiki/Feedforward_neural_network
[Rprop]: https://en.wikipedia.org/wiki/Rprop
[encog]: https://github.com/jeffheaton/encog-java-core
[AF]: https://en.wikipedia.org/wiki/Activation_function

## Demonstration

{% include youtube.html id="jXXJkbh0gwI" %}

## Track Results

<div class="table-responsive" markdown="1">
| Track         | Laps | Time (m:s:ms) | Top speed (km/h) |
|:--------------|:-----|:--------------|:-----------------|
| Alpine 1      | 2    | 05:34:580     | 241              |
| Alpine 2      | 1    | 02:54:160     | 217              |
| F-speedway    | 3    | 02:29:860     | 290              |
{:.table}
</div>
