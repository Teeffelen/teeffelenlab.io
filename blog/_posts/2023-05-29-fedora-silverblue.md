---
layout: post
title: "Fedora Silverblue"
description: "The immutable future?"
tags: [Fedora, Silverblue, Linux]
---

I've been using [Fedora Workstation] as my main operating system for several
years now. But the Fedora project also ships an immutable version of their
OS called "Silverblue".

Fedora explains their immutable variant as follows:

> An immutable variant looks, feels, and behaves just like a regular desktop
> operating system, but your updates are delivered as full images of a working
> system. This makes every installation identical to every other, and it will
> never change while in use.
>
> What's more, Silverblue will always keep an older
> version of the system around for you to boot back into, should you need to,
> allowing you to try new programs, desktops, or even complete version
> upgrades fearlessly!
>
> -- [_Fedora Project_][fedora-silverblue]

This system is almost the opposite of a rolling release distribution such as
[Arch Linux] -- which I used as my daily OS before Fedora Workstation, where
updates are small and frequent, and where each application can modify the root
config files when needed.

I wanted to tryout this immutable variant, to see the pros and cons of a
immutable OS for myself, so I went ahead and spun up Fedora Silverblue in a
virtual machine using [Gnome Boxes].

## Installation

I downloaded [Fedora Silverblue 38][fedora-silverblue], which was released on
the 18th of April, 2023.

As for the VM, I went with 25 GB's of storage combined with 6 GB's of system
memory, which should be plenty to tryout Silverblue and _install some
applications_ -- more about this later.

The initial setup and installation process reminded me very much of the Fedora
Workstation process, in fact, I could not notice this immutable future at all
during installation.

Sadly, the installer included with Fedora 38 is still one of the worst user
experiences when it comes down to Linux installers
-- But a more [noob friendly web installer is in the works][web-installer]!

[web-installer]:https://news.itsfoss.com/fedora-new-web-ui-install-dev/

## First boot

After the first boot, we are asked to enable some location services and third
party repositories, just like the Workstation variant.

We also get to setup our user account, after which we are greeted with a
(very clean and almost untouched) Gnome Desktop. The pre-installed applications
are, as far as I can tell, the same as those found on Fedora Workstation.

One notable thing is that the included version of Firefox appears to be shipped
as an RPM package, rather than a containerized flatpak
-- which is odd for a immutable and container focussed distribution.

## Command not found

Hmm, it looks like `htop` is not shipped by default, let me install it really
quick using the `dnf` package manager:

![command not found](/assets/img/command-not-found.png){:width="856px" height="212px"}

And this is where we finally encounter the "immutable" part of Fedora
Silverblue. Because the root of the OS is immutable, the `dnf` package manager
-- or any oher package manager using root -- will not be able to write packages.

This is the advantage of an immutable OS. Because it is read only, no user,
or application, can effect the system binaries and their configuration files.
Which keeps the base system extremely stable and secure.

> _"But how do I install packages without my trusted package manager?"_

Well for most graphical applications we have [Flatpaks], which are containerized
applications installed directly to the user's home directory.
But other packages, such as command line tools, will have to be layed on top of
our current Fedora Silverblue image, using the `rpm-ostree` command.

## Installing Gnome Tweaks

As explained earlier, in order to install packages on an immutable OS, we need
to install them on top of the base image using `rpm-ostree`. After which we will
have to reboot into the newly generated image, which includes our package.

To tryout this process of installing packages, I decided to install
`gnome-tweaks`, as it is not available as a Flatpak. Which leaves us with one
option:

```bash
rpm-ostree install gnome-tweaks
```

Notice how the command above did not include `sudo`? That is because we are not
directly installing Gnome Tweaks on the currently running image.

Instead, we rebuild the base image and include Gnome Tweaks on top of it.
After switching to this new image using a simple reboot, we can see and use our
new package as expected.

## Lighting fast updates

Another advantage of an immutable Linux distribution, is that switching
images is nearly instantaneous. Which makes updates almost unnoticeable,
especially when newer images can be downloaded automatically in the background.

The downside being that a reboot is required each time a update needs to be
"applied". Which can prove to be quite annoying if you do not power off/reboot
your system regularly.

## Go immutable?

During my time with Fedora Silverblue, I hardly noticed the immutable nature of
the system. Instead, I found myself installing and using applications as I
would on any other system.

I liked the fact that the core system cannot change or break when in use
-- keep in mind that Flatpak applications can still change when in use.

This stability, combined with the option to rollback to previous images, reliefs
a lot of stress from maintaining your OS, and lets you focus more on using your
PC.

I am really interested to see the future of these immutable Linux distributions,
as other immutable systems such as: Android, MacOS and ChromeOS already prove
to be quite stable and successful.

> I would recommend giving [Fedora Silverblue][fedora-silverblue] a try! 🐧

[Fedora Workstation]: https://fedoraproject.org/workstation/
[fedora-silverblue]: https://fedoraproject.org/silverblue/
[Arch Linux]: https://archlinux.org/
[Gnome Boxes]: https://apps.gnome.org/app/org.gnome.Boxes/
[Flatpaks]: https://www.flatpak.org/
