---
layout: post
title: "Talks and Videos"
description: "Interesting and inspiring (tech) talks"
tags: [talks, videos, tech]
---

A list of talks, videos and websites that I've found interesting or inspiring.

## Talks

[How to Speak](https://www.youtube.com/watch?v=Unzc731iCUY)
: Patrick Winston on how to improve your speaking ability.

[How do GPS/Galileo really work](https://www.youtube.com/watch?v=XjRHl2Rr9tQ)
: An interesting talk about GPS/Galileo by Bert Hubert.

[How technology loses out in companies, countries & continents](https://www.youtube.com/watch?v=PQccNdwm8Tw)
: Presentation on how companies tend to lose their technological edge and devolve into outsourcing hubs.

[Wat](https://www.destroyallsoftware.com/talks/wat)
: A lightning talk by Gary Bernhardt about the "wat".

## Videos

[Edsger W. Dijkstra on Dutch TV](https://www.youtube.com/watch?v=RCCigccBzIU)
: "Thinking As Discipline" with Edsger Dijkstra (English Subtitles).

[The Best (and Worst) Ways to Shuffle Cards](https://www.youtube.com/watch?v=AxJubaijQbI)
: A fun view on how to properly shuffle a deck of playing cards.

[Catch-22 Logical Paradox | Gentleman Thinker](https://www.youtube.com/watch?v=5RkJuum3FvE)
: The Catch-22 Paradox explained in brief in a posh British accent.

[Bullshit Jobs with David Graeber on Dutch TV](https://www.npostart.nl/vpro-backlight/24-03-2019/WO_VPRO_15614055)
: Jobs that do not bring any satisfaction or any other benefit (English Subtitles).

## Websites

[tylervigen.com](https://tylervigen.com/spurious-correlations)
: View some spurious correlations of seemingly unrelated items or events.

[thetruesize.com](https://thetruesize.com/)
: Find out the true size of a country by dragging them around the globe.

[app.traveltime.com](https://app.traveltime.com/)
: Create a travel time map based on a method of transportation.

[webkay.robinlinus.com](https://webkay.robinlinus.com/)
: Discover what every browser knows about you.

[visuwords.com](https://visuwords.com)
: Go word spelunking and discover the dictionary visually.

[namecheap.com](https://www.namecheap.com/logo-maker/)
: Create your next logo for free with this handy tool by NameCheap.
