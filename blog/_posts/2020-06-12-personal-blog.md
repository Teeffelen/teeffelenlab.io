---
layout: post
title: "Personal Blog"
description: "How I setup this site over time"
tags: [jekyll, blog]
---

This site started as a small experiment to try [GitLab's CI/CD feature].
The backend is based on [Jekyll], a static site generator used to convert
markdown files into HTML files for the web.

It is based on an example template I came across on [GitLab pages].
I expanded this template by including an awesome CSS framework called
[Halfmoon] --with built-in dark mode support!

I have since moved to using [new.css] for the frontend, as it focuses on
styling the bare HTML tags without using mountains of CSS classes everywhere.
The main inspiration for this site is from [Kai Hendry's blog]
--which is worth checking out when you're done!

[GitLab's CI/CD feature]: https://docs.gitlab.com/ee/ci/
[Jekyll]: https://jekyllrb.com/
[GitLab pages]: https://gitlab.com/pages/jekyll
[Halfmoon]: https://www.gethalfmoon.com/
[new.css]: https://github.com/xz/new.css
[Kai Hendry's blog]: https://dabase.com
