---
layout: post
title: "Markdown Elements"
description: "Some description"
tags: [Markdown]
---

A paragraph looks like this -- Lorem ipsum dolor sit amet, consectetur
adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna
aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi
ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in
voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint
occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit
anim id est laborum.

# Heading 1

## Heading 2

### Heading 3

#### Heading 4

##### Heading 5

###### Heading 6

## Text options

Emphasis, aka italics, with single *asterisks* or _underscores_.

Strong emphasis, aka bold, with double **asterisks** or __underscores__.

Combined emphasis with **_double asterisks and single underscores_**.

Strikethrough uses two tildes, ~~scratch this~~.

Abbreviations such as HTML will be explained later.

## Lists

Ordered list:

1. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod.
1. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod.
    1. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod.

Unordered list:

- Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod.
- Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod.
  - Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod.

## Links

[Google it yourself](https://www.google.com/)

[Need a hint?](https://www.google.com/ "This link also goes to google")

A footnote can also be usefull [^1]. You still need to google that [^google].

[^1]: Read more about [Hobbits](https://en.wikipedia.org/wiki/Hobbit) on WikiPedia
[^google]: Just a demo

## Quotes

A simple quote looks like this:

> Simple and short quote, **look!**

A longer quote:

> The longer I live, the more I realize that I am never wrong about anything.
>
> And that all the pains I have so humbly taken to verify my
> notions have only wasted my time!

## Terms

term
: definition
: another definition

another term
and another term
: and a definition for the term

## Code

Inline code has `back-ticks around` it.

Code block with language highlighting:

```python
import something

def some_func(a, b, c):
    return a + b + c

while True:
    x = some_func(1, 2, 3)
    print(x)
```

## Tables

Simple table:

| A simple | table |
| with multiple | lines|

Striped table:

| Two           | Columns       |
| :------------ |:--------------|
| Both          | left-aligned  |
| Lorem         | Ipsum         |
| zebra stripes | are neat      |

Responsive bordered table:

<div class="table-responsive" markdown="1">

| Default aligned |Left aligned| Center aligned  | Right aligned  |
|-----------------|:-----------|:---------------:|---------------:|
| First body part |Second cell | Third cell      | fourth cell    |
| Second line     |foo         | **strong**      | baz            |
| Third line      |quux        | baz             | bar            |

</div>

## Videos

{% include youtube.html id="jXXJkbh0gwI" %}

## Extensions

This is a paragraph
{::comment}
This is a comment which is completely ignored.
{:/comment}
... paragraph continues here.

Extensions can also be used
inline {::nomarkdown}**see**{:/}!

## Abbreviations

This is an HTML
example.

*[HTML]: Hyper Text Markup Language

## References
